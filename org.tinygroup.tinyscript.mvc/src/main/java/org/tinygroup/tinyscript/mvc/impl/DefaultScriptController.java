package org.tinygroup.tinyscript.mvc.impl;

import org.tinygroup.tinyscript.config.FunctionConfig;
import org.tinygroup.tinyscript.mvc.RequestMapping;
import org.tinygroup.tinyscript.mvc.ScriptController;

public class DefaultScriptController implements ScriptController{

    private RequestMapping requestMapping;
	  
    private String scriptClassName;
    
    private String functionName;
	  
	private FunctionConfig functionConfig;
	
	public DefaultScriptController(RequestMapping requestMapping,String scriptClassName,FunctionConfig functionConfig){
		this.requestMapping = requestMapping;
		this.scriptClassName = scriptClassName;
		this.functionConfig = functionConfig;
	}
	
	public DefaultScriptController(RequestMapping requestMapping,String scriptClassName,String functionName){
		this.requestMapping = requestMapping;
		this.scriptClassName = scriptClassName;
		this.functionName = functionName;
	}
	
	public RequestMapping getRequestMapping() {
		return requestMapping;
	}

	public String getScriptClassName() {
		return scriptClassName;
	}

	public FunctionConfig getFunctionConfig() {
		return functionConfig;
	}

	public String getFunctionName() {
		return functionConfig==null?functionName:functionConfig.getName();
	}

	public String getUrl() {
		if(requestMapping!=null && requestMapping.getPaths()!=null && requestMapping.getPaths().length>0){
		   return requestMapping.getPaths()[0];
		}
		return null;
	}
	  
	  
}
