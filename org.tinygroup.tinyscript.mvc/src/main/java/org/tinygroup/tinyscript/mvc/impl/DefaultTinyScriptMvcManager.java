package org.tinygroup.tinyscript.mvc.impl;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.tinygroup.tinyscript.ScriptSegment;
import org.tinygroup.tinyscript.config.FunctionConfig;
import org.tinygroup.tinyscript.config.ScriptClassConfig;
import org.tinygroup.tinyscript.mvc.RequestMapping;
import org.tinygroup.tinyscript.mvc.ScriptController;
import org.tinygroup.tinyscript.mvc.TinyScriptMvcManager;
import org.tinygroup.tinyscript.mvc.config.ScriptControllerConfig;
import org.tinygroup.tinyscript.mvc.config.ScriptControllerConfigs;

public class DefaultTinyScriptMvcManager implements TinyScriptMvcManager{
    
	private Map<Object,ScriptControllerConfig> controllerMap = new HashMap<Object,ScriptControllerConfig>();
	private Map<String,ScriptClassConfig> scriptClassMap = new HashMap<String,ScriptClassConfig>();
	
	public List<ScriptController> getScriptControllerList() {
		List<ScriptController> list = new ArrayList<ScriptController>();
		//包装服务配置
		for(ScriptClassConfig scriptClassConfig:scriptClassMap.values()){
			for(FunctionConfig functionConfig:scriptClassConfig.getFunctions()){
				RequestMapping requestMapping = new DefaultRequestMapping(new String[]{createUrl(scriptClassConfig,functionConfig)});  
				list.add(new DefaultScriptController(requestMapping,scriptClassConfig.getScriptClassName(),functionConfig));
			}
		}
		return list;
	}
	
	/**
	 * 定义脚本服务的唯一URL路径<br/>
	 * @param scriptClassConfig
	 * @param functionConfig
	 * @return
	 */
	protected String createUrl(ScriptClassConfig scriptClassConfig,FunctionConfig functionConfig){
		return "/"+scriptClassConfig.getScriptClassName()+"/"+functionConfig.getName();
	}

	public void addScriptClassConfig(ScriptSegment segment) {
		scriptClassMap.put(segment.getSegmentId(), new DefaultScriptSegmentConfig(segment));
	}

	public void removeScriptClassConfig(ScriptSegment segment) {
		scriptClassMap.remove(segment.getSegmentId());
	}


	public void addScriptControllerConfigs(ScriptControllerConfigs configs) {
		if(configs!=null && configs.getControllers()!=null){
		   for(ScriptControllerConfig config:configs.getControllers()){
			   addScriptControllerConfig(config);
		   }
		}
	}

	public void addScriptControllerConfig(ScriptControllerConfig config) {
        if(config!=null){
           controllerMap.put(getKey(config), config);
        }
	}

	public void removeScriptControllerConfigs(ScriptControllerConfigs configs) {
        if(configs!=null && configs.getControllers()!=null){
           for(ScriptControllerConfig config:configs.getControllers()){
 			   removeScriptControllerConfig(config);
 		   }
		}
	}

	public void removeScriptControllerConfig(ScriptControllerConfig config) {
		if(config!=null){
		   controllerMap.remove(getKey(config));
        }
	}
	
	private Object getKey(ScriptControllerConfig config){
		return config.getScriptClassName()+"@"+config.getFunctionName(); 
	}

	public List<ScriptControllerConfig> getScriptControllerConfigList() {
		return new ArrayList<ScriptControllerConfig>(controllerMap.values());
	}

	public ScriptControllerConfig getScriptControllerConfig(String className,
			String functionName) {
		return  controllerMap.get(className+"@"+functionName);
	}

}
