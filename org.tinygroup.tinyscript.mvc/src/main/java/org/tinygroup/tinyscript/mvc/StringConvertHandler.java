package org.tinygroup.tinyscript.mvc;

import javax.servlet.http.HttpServletResponse;

/**
 * 文本转换
 * @author yancheng11334
 *
 * @param <Accept>
 */
public abstract class StringConvertHandler<Accept> implements ConvertHandler<Accept> {
    
	protected abstract String convert(Object obj,Accept accept);
	
	protected abstract String getContentType();
	
	public void convert(HttpServletResponse response,Object obj,Accept accept) throws Exception{
		String text = convert(obj,accept);
		response.setContentType(getContentType());
		response.getWriter().write(text);
	}
}
