package org.tinygroup.tinyscript.expression.convert;

import java.math.BigDecimal;

import org.tinygroup.tinyscript.expression.Converter;

public class BigDecimalFloat implements Converter<BigDecimal,Float> {

	public Float convert(BigDecimal object) {
		return object.floatValue();
	}

	public Class<?> getSourceType() {
		return BigDecimal.class;
	}

	public Class<?> getDestType() {
		return Float.class;
	}

}
